package com.algowork.assignment.activities;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.algowork.assignment.R;
import com.algowork.assignment.adapters.ProductAdapter;
import com.algowork.assignment.database.DatabaseConnection;
import com.algowork.assignment.database.DummyData;
import com.algowork.assignment.models.Product;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerview;
    private ProductAdapter productAdapter;
    private ArrayList<Product> productArrayList;
    ProgressBar progressBar;
    DatabaseConnection databaseConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progressbar);
        databaseConnection = new DatabaseConnection(this);
        setupRecyclerView();
        DummyData dummyData = new DummyData(databaseConnection);
        dummyData.addDummyData();
        getDummyData();

    }


    private void setupRecyclerView() {

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        productArrayList = new ArrayList<>();
        productAdapter = new ProductAdapter(productArrayList, this);
        recyclerview.setAdapter(productAdapter);
    }


    private void getDummyData() {

        Cursor cursor = databaseConnection.getProducts();


        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                Product product = new Product(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseConnection.FIELD_NAME)), cursor.getString(cursor.getColumnIndexOrThrow(DatabaseConnection.FIELD_CATEGORY)), cursor.getString(cursor.getColumnIndexOrThrow(DatabaseConnection.FIELD_IMAGE)), cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseConnection.FIELD_PRICE)));
                productArrayList.add(product);
                cursor.moveToNext();
            }


        }

        productAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }
}
