package com.algowork.assignment.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.algowork.assignment.R;
import com.algowork.assignment.models.Product;
import com.bumptech.glide.Glide;

public class ProductDetailActivity extends AppCompatActivity {

    private Product product;
    private TextView productName_tv, category_tv, productPrice_tv;
    private ImageView productImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        productName_tv = findViewById(R.id.tv_product_name);
        category_tv = findViewById(R.id.tv_product_category);
        productPrice_tv = findViewById(R.id.tv_price);
        productImageView = findViewById(R.id.productimage);
        if (getIntent() != null) {
            product = getIntent().getParcelableExtra("product");
        }

        if (product != null) {
            initView();
        }

    }


    private void initView() {
        productPrice_tv.setText("₹" + product.getPrice());
        productName_tv.setText(product.getName());
        category_tv.setText(product.getCategory());

        Glide.with(this).load(product.getImageurl()).placeholder(R.drawable.noimage).into(productImageView);

    }

}
