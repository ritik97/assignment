package com.algowork.assignment.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.algowork.assignment.R;
import com.algowork.assignment.activities.ProductDetailActivity;
import com.algowork.assignment.models.Product;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<Product> productList;
    private Context context;


    public ProductAdapter(List<Product> productList, Context context) {
        this.productList = productList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final Product product = productList.get(i);

        viewHolder.tv_price.setText("₹" + product.getPrice());
        viewHolder.tv_product_name.setText(product.getName());
        viewHolder.tv_product_category.setText(product.getCategory());

        Glide.with(context).load(product.getImageurl()).placeholder(R.drawable.noimage).into(viewHolder.productimage);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("product", product);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_product_category, tv_product_name, tv_price;
        private ImageView  productimage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_product_category = itemView.findViewById(R.id.tv_product_category);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_price = itemView.findViewById(R.id.tv_price);
            productimage = itemView.findViewById(R.id.productimage);

        }
    }


}
