package com.algowork.assignment.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseConnection extends SQLiteOpenHelper {
    SQLiteDatabase sq;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "algowork.db";
    public static final String TABLE_NAME = "Product";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_IMAGE = "imageurl";
    public static final String FIELD_CATEGORY = "category";
    public static final String FIELD_PRICE = "price";


    private static final String CREATE_TABLE_PRODUCT = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + FIELD_NAME + " text ,"
            + FIELD_CATEGORY + " text , "
            + FIELD_IMAGE + " text , "
            + FIELD_PRICE + " int"
            + " );";


    public DatabaseConnection(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sq) {

        sq.execSQL(CREATE_TABLE_PRODUCT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    public void insertProducts(String name, String category, int price, String url) {
        sq = this.getWritableDatabase();


        ContentValues contentValues = new ContentValues();
        contentValues.put(FIELD_NAME, name);
        contentValues.put(FIELD_CATEGORY, category);
        contentValues.put(FIELD_IMAGE, url);
        contentValues.put(FIELD_PRICE, price);
        sq.insert(TABLE_NAME, null, contentValues);


    }

    public Cursor getProducts() {
        sq = this.getReadableDatabase();
        Cursor res = sq.rawQuery("select * from " + TABLE_NAME, null);
        return res;

    }


public  void deleteProducts()
{   sq = this.getWritableDatabase();
            sq.execSQL("delete from "+TABLE_NAME);
}

}