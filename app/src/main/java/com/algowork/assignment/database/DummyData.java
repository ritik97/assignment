package com.algowork.assignment.database;

import android.database.Cursor;

import com.algowork.assignment.adapters.ProductAdapter;
import com.algowork.assignment.models.Product;

import java.util.ArrayList;
import java.util.List;

public class DummyData {


    private DatabaseConnection databaseConnection;
    private List<Product> productList;

    public DummyData(DatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
        productList = new ArrayList<>();
    }


    public void addDummyData() {

databaseConnection.deleteProducts();
        databaseConnection.insertProducts("Lenovo Desktop", "Electronics", 45000, "https://static.pexels.com/photos/204611/pexels-photo-204611-medium.jpeg");
        databaseConnection.insertProducts("Samsung Mobile", "Electronics", 25000, "https://static.pexels.com/photos/214487/pexels-photo-214487-medium.jpeg");
        databaseConnection.insertProducts("Cobina Camera", "Electronics", 5000, "https://static.pexels.com/photos/168575/pexels-photo-168575-medium.jpeg");
        databaseConnection.insertProducts("Tablet Pixel", "Electronics", 9000, "https://static.pexels.com/photos/213384/pexels-photo-213384-medium.jpeg");
        databaseConnection.insertProducts("Happy", "Books", 400, "https://static.pexels.com/photos/67442/pexels-photo-67442-medium.jpeg");
        databaseConnection.insertProducts("Mystery", "Books", 800, "https://static.pexels.com/photos/159494/book-glasses-read-study-159494-medium.jpeg");
        databaseConnection.insertProducts("Wining of Barbara", "Books", 700, "https://static.pexels.com/photos/33283/stack-of-books-vintage-books-book-books-medium.jpg");
        databaseConnection.insertProducts("Everything", "Books", 400, "https://static.pexels.com/photos/38167/pexels-photo-38167-medium.jpeg");
        databaseConnection.insertProducts("Punjabi Suit", "Lifestyles", 4500, "https://static.pexels.com/photos/169047/pexels-photo-169047-medium.jpeg");
        databaseConnection.insertProducts("Dress Bounce", "Lifestyles", 5000, "https://static.pexels.com/photos/160826/girl-dress-bounce-nature-160826-medium.jpeg");
        databaseConnection.insertProducts("Bow Tie", "Lifestyles", 600, "https://static.pexels.com/photos/1702/bow-tie-businessman-fashion-man-medium.jpg");
        databaseConnection.insertProducts("Children Wear", "Lifestyles", 700, "https://static.pexels.com/photos/35188/child-childrens-baby-children-s-medium.jpg");


    }


}
